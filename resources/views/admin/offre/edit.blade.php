@extends('page.layout')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Modifier mission</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('missions.index') }}"> Retour</a>
            </div>
        </div>
    </div>


    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Oooops!</strong><br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form action="{{ route('missions.update',$mission->id) }}" method="POST">
    	@csrf
        @method('PUT')


         <div class="row">
         <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Titre audit :</strong>
		            <input type="text" name="titre_audit"  value="{{ $mission->titre_audit }}" class="form-control" placeholder="">
		        </div>
		    </div>
		    
            <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Responsable :</strong>
		            <input type="text" name="responsable" value="{{ $mission->responsable }}" class="form-control" placeholder="">
		        </div>
		    </div>
		    
            <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Date de debut :</strong>
		            <input type="date" name="date_debut"  value="{{ $mission->date_debut }}" class="form-control" placeholder="">
		        </div>
		    </div>
		    <div class="col-xs-12 col-sm-12 col-md-12">
		        <div class="form-group">
		            <strong>Date d'echeance :</strong>
		            <input type="date" name="date_fin" value="{{ $mission->date_fin }}" class="form-control" placeholder="">
		        </div>
		    </div>
		    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
		      <button type="submit" class="btn btn-primary">Valider
              </button>
		    </div>
		</div>


    </form>

@endsection