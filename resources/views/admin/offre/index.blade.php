@extends('admin.layouts.app')

@section('content')
  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"> <i class="fas fa-fw fa-home"></i>Offres immobilières</h1>
  </div>

  @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
  @endif


  <!-- DataTales Example style="background-color:white;"-->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold">
        Affichages des offres immobilières
        <span style="float:right">
          <a style="color:gray" href="{{ route('offre.create') }}">
            <i class="fas fa-fw fa-home"></i>
          </a>
        </span>
      </h6>
    </div>
    <div class="card-body">
    <div class="table-responsive">
      <table class="table table-hover" id="dataTable" width="100%" >
          <thead>
            <tr>
              <th>ID</th>
              <th>Type</th>
              <th>Libelle</th>
              <th>Superficie</th>
              <th>S. Géo</th>
              <th>Montant</th>
              <th>Photo 1</th>
              <th>Photo 2</th>
              <th>Photo 3</th>
              <th>Photo 4</th>
              <th>Description</th>
              <th width="10%">Action</th>
            </tr>
          </thead>
          <tbody>
          @foreach ($data as $key => $offre)
            <tr>
              <td>{{ $offre->id }}</td>
              <td>{{ $offre->type }}</td>
              <td>{{ $offre->designation }}</td>
              <td>{{ $offre->superficie }}</td>
              <td>{{ $offre->situation_geo }}</td>
              <td>{{ $offre->montant }}</td>
              <td><img width="40 px" src="../../images/{{ $offre->image_un }}"></td>
              <td><img width="40 px" src="../../images/{{ $offre->image_deux }}"></td>
              <td><img width="40 px" src="../../images/{{ $offre->image_trois }}"></td>
              <td><img width="40 px" src="../../images/{{ $offre->image_q }}"></td>
              <td>{{ $offre->detail }}</td>
              <td>
                <form action="{{ route('offre.destroy',$offre->id) }}" method="POST">
                  @csrf
                    @method('DELETE')
                  <button type="submit" class="btn btn-danger"><i  class="fas fa-trash"></i></button>
                </form>
              </td>
            </tr>
          @endforeach
          </tbody>     
      </table>
    </div>
    </div>
  </div>
@endsection