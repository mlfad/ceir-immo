@extends('admin.layouts.app')


@section('content')
   <!-- Page Heading -->


	<div class="col-lg-12">
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Recommandations</h1>
    <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>-->
</div>
		@if ($errors->any())
			<div class="alert alert-danger">
				<strong>Ooops!</strong> <br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold">Ajouter une recommandation</h6>
            </div>
            <div class="card-body">
              <form  action="{{ route('offre.store') }}" method="post"  enctype="multipart/form-data">
              @csrf
                <div class="form-group row">
                  <label for="inputPassword3"  class="col-sm-3 col-form-label">Type</label>
                  <div class="col-sm-9">
                    <select id="inputState" name="type" class="form-control">
                      <option value="logement">Logement</option>
                      <option value="terrain">Terrain</option>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-3 col-form-label">Libelle</label>
                  <div class="col-sm-9">
                    <input type="text"  name="designation" class="form-control" id="inputEmail3">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-3 col-form-label">Superficie</label>
                  <div class="col-sm-9">
                    <input type="text"  name="superficie" class="form-control" id="inputEmail3">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputPassword3" class="col-sm-3 col-form-label">Montant</label>
                  <div class="col-sm-9">
                    <input type="text" name="montant" class="form-control" id="inputPassword3">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-3 col-form-label">S. geographique</label>
                  <div class="col-sm-9">
                    <input type="text" name="situation_geo" class="form-control" id="inputEmail3">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputPassword3" class="col-sm-3 col-form-label">Photo principale</label>
                  <div class="col-sm-9">
                    <input type="file" name="image_un" class="form-control" id="inputPassword3">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-3 col-form-label">Detail photo 1</label>
                  <div class="col-sm-9">
                    <input type="file" name="image_deux" class="form-control" id="inputEmail3">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail3" class="col-sm-3 col-form-label">Detail photo 2</label>
                  <div class="col-sm-9">
                    <input type="file" name="image_trois" class="form-control" id="inputEmail3">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputPassword3" class="col-sm-3 col-form-label">Detail photo 3</label>
                  <div class="col-sm-9">
                    <input type="file" name="image_q" class="form-control" id="inputPassword3">
                  </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-3 col-form-label">Description</label>
                    <div class="col-sm-9">
                      <textarea  id="message" name="detail" class="form-control"></textarea>
                    </div>
                </div>
                
                
                <div class="form-group row">
                  <div class="col-sm-12">
                    <button style="float : right;" type="submit" class="btn btn-primary">Valider</button>
                  </div>
                </div>
              </form>
              </div>
		</div>
	</div>
@endsection
