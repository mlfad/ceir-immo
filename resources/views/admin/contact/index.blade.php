@extends('admin.layouts.app')

@section('content')
  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"> <i class="fas fa-fw fa-users"></i> Couriels</h1>
  </div>

  @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
  @endif

  <!-- DataTales Example style="background-color:white;"-->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold">
        Affichage des couriels
      </h6>
    </div>
    <div class="card-body">
    <div class="table-responsive">
      <table class="table table-hover" id="dataTable" width="100%" >
          <thead>
            <tr>
              <th>ID</th>
              <th>Nom</th>
              <th>Email</th>
              <th>Contact</th>
              <th>Site internet</th>
              <th>Commentaire</th>
              <th width="10%">Action</th>
            </tr>
          </thead>
          <tbody>
          @foreach ($data as $key => $contact)
            <tr>
              <td>{{ $contact->id }}</td>
              <td>{{ $contact->name }}</td>
              <td>{{ $contact->email }}</td>
              <td>{{ $contact->phone }}</td>
              <td>{{ $contact->url }}</td>
              <td>{{ $contact->comment }}</td>
              <td>
                <form action="{{ route('contact.destroy',$contact->id) }}" method="POST">
                  @csrf
                    @method('DELETE')
                  <button type="submit" class="btn btn-danger"><i  class="fas fa-trash"></i></button>
                </form>
              </td>
            </tr>
          @endforeach
          </tbody>     
      </table>
    </div>
    </div>
  </div>
@endsection