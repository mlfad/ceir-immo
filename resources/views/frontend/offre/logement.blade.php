<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from trendytheme.net/demo/matrox/construction-index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 15 Nov 2018 09:26:48 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="materialize is a material design based mutipurpose responsive template">
<meta name="keywords" content="material design, card style, material template, portfolio, corporate, business, creative, agency">
<meta name="author" content="trendytheme.net">
<title>CEIR - Immobilier - Transport</title>

<link rel="shortcut icon" href="../assets/img/ico/favicon.jpg">

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/img/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/img/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/img/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="../assets/img/ico/apple-touch-icon-57-precomposed.png">
<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500,700,900' rel='stylesheet' type='text/css'>

<link href="../assets/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">

<link href="../assets/fonts/iconfont/material-icons.css" rel="stylesheet">

<link href="../assets/magnific-popup/magnific-popup.css" rel="stylesheet">

<link href="../assets/flexSlider/flexslider.css" rel="stylesheet">

<link href="../assets/materialize/css/materialize.min.css" rel="stylesheet">

<link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<link href="../assets/css/shortcodes/shortcodes.css" rel="stylesheet">

<link href="../style.css" rel="stylesheet">

<link href="../assets/css/skins/construction.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="../assets/revolution-version4/css/extralayers.css" media="screen">
<link rel="stylesheet" type="text/css" href="../assets/revolution-version4/css/settings.css" media="screen">


<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body id="top">
<header id="header" class="tt-nav nav-center-align">
    <div class="light-header">
        <div class="container mainmenu">
            <div id="materialize-menu" class="menuzord">
            <!--a href="index.html" class="logo-brand">
            <img src="assets/img/construction/logo.png" alt="" />
            </a-->
            <ul class="menuzord-menu border-top" id="menu-list">
                        <li style='float:left'><a href="#"><img style="height:100px;" src="../assets/img/construction/logo.png" alt="" /></a></li>
                        <li style='padding-top:30px'><a href="http://ceir-immo.test/">ACCUEIL</a></li>
                        <li style='padding-top:30px'><a href="{{ route('presentation.talk') }}">PRESENTATION</a>
                        </li>
                        <li style='padding-top:30px' class="active"><a href="javascript:void(0)">NOS OFFRES</a>
                            <ul class="dropdown">
                                <li><a href="{{ route('offre.terrain') }}">Offres terrains</a></li>
                                <li><a href="{{ route('offre.logement') }}">Offres logements</a></li>
                            </ul>
                        </li>
                        <li style='padding-top:30px'><a href="construction-contact.html">PROJETS</a></li>
                        <li style='padding-top:30px'><a href="{{ route('contact.contact-us') }}">CONTACTS</a></li>
                        <!--li style='padding-top:30px'><a href="">FAQS</a></li-->
                        <li style='float:right; padding-top:27px;'><button type="button"  class="btn btn-primary btn-sm">RESERVATION</button></li>
                    </ul>
            </div>
            </div>
        </div>

        <div class="menu-appear-alt">
            <div class="container">
                <div id="materialize-menu-alt" class="menuzord">
                </div>
            </div>
        </div>

    </div>
</header>

<section class="page-title page-title-bg ptb-70">
<div class="container">
<div class="row">
<div class="col-md-12">
<br/>
<h2 class="text-uppercase"></h2>
<span></span>
<ol class="breadcrumb">
<li><a href="#"></a></li>
<li class="active"></li>
</ol>
</div>
</div>
</div>
</section>

<section class="section-padding  padding-bottom-110">
<div class="container">
<div class="row">
@foreach ($data as $key => $offre)
<div class="col-md-4">
    <article class="card">
        <div class="card-image waves-effect waves-block waves-light">
             <img class="activator" src="../images/{{ $offre->image_un }}">
        </div>
        <div class="card-content">
            <span class="entry-title activator text-medium">{{ $offre->montant }}</span><br>
            <span class="entry-title activator text-medium">{{ $offre->designation }} - {{ $offre->situation_geo }}</span><br>
            <span class="entry-title activator text-medium">{{ $offre->detail }}</span>
        </div>
    </article>
</div>
@endforeach
</div>
</div>
</section>
<footer class="footer footer-one">
<div class="primary-footer dark-bg lighten-3">
<div class="container">
<a href="#top" class="page-scroll btn-floating btn-large brand-bg back-top waves-effect waves-light tt-animate btt" data-section="#top">
<i class="material-icons">&#xE316;</i>
</a>
<div class="row">
<div class="col-md-4 widget clearfix">
<h2 class="white-text">Qui sommes-nous?</h2>
<p style="text-align:justify;">CEIR est une compagnie ivoirienne spécialisée dans l'immobilier et le transport.
 Notre mission est d’offrir à chacun de nos clients, une expérience immobilière simple et efficace afin qu’ils concrétisent leurs projets d’achat, de vente ou de location en toute sérénité. Avec une équipe de  professionnels de l’immobilier, nous mettons à dispositions le plus large choix d’annonces afin de leur faciliter la recherche d’un bien selon leurs critères propres, et répondre à toutes les questions soulevées par la réalisation d’un projet immobilier.</p>
<ul class="social-link tt-animate ltr">
<li><a href="#"><i class="fa fa-facebook"></i></a></li>
<li><a href="#"><i class="fa fa-twitter"></i></a></li>
<li><a href="#"><i class="fa fa-tumblr"></i></a></li>
<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
<li><a href="#"><i class="fa fa-instagram"></i></a></li>
<li><a href="#"><i class="fa fa-rss"></i></a></li>
</ul>
</div>
<div class="col-md-3 widget">
<h2 class="white-text">Liens important</h2>
<ul class="footer-list">
<li><a href="#">Présentation</a></li>
<li><a href="#">Nos Offres</a></li>
<li><a href="#">Nos Projets</a></li>
<li><a href="#">Reservations</a></li>
<li><a href="#">Contacts-Nous</a></li>
</ul>
</div>
<div class="col-md-5">
<h2 class="white-text">Suivez nous</h2>
<form name="contact-form" id="contactForm" action="https://trendytheme.net/demo/matrox/sendemail.php" method="POST">
<div class="row">
<div class="col-md-6">
<div class="input-field">
<input type="text" name="name" class="validate" id="name">
<label for="name">Nom</label>
</div>
</div>
<div class="col-md-6">
<div class="input-field">
<label class="sr-only" for="email">Email</label>
<input id="email" type="email" name="email" class="validate">
<label for="email" data-error="wrong" data-success="right">Email</label>
</div>
</div>
</div>
<div class="row">
<div class="col-md-6">
<div class="input-field">
<input id="phone" type="tel" name="phone" class="validate">
<label for="phone">Numero de téléphone</label>
</div>
</div>
<div class="col-md-6">
<div class="input-field">
<input id="website" type="text" name="website" class="validate">
<label for="website">Votre site web</label>
</div>
</div>
</div>
<div class="input-field">
<textarea name="message" id="message" class="materialize-textarea"></textarea>
<label for="message">Message</label>
</div>
<button type="submit" name="submit" class="waves-effect waves-light btn submit-button pink mt-30">Envoyer Message</button>
</form>
</div>
</div>
</div>
</div>
<div class="secondary-footer dark-bg darken-1" style="text-align:center;">
<div class="container">
<span class="copy-text">Copyright &copy; 2020 <a href="#">CEIR</a> &nbsp; | &nbsp; Tous droits reservés &nbsp; | &nbsp; Developpé par <a href="#">oora.solution</a></span>
</div>
</div>
</footer>

<div id="preloader">
<div class="preloader-position">
<img src="../assets/img/logo-colored.jpg" alt="logo">
<div class="progress">
<div class="indeterminate"></div>
</div>
</div>
</div>


<script src="../assets/js/jquery-2.1.3.min.js"></script>
<script src="../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../assets/materialize/js/materialize.min.js"></script>
<script src="../assets/js/jquery.easing.min.js"></script>
<script src="../assets/js/smoothscroll.min.html"></script>
<script src="../assets/js/menuzord.js"></script>
<script src="../assets/js/equalheight.js"></script>
<script src="../assets/js/bootstrap-tabcollapse.min.js"></script>
<script src="../assets/js/jquery.inview.min.js"></script>
<script src="../assets/js/jquery.countTo.min.js"></script>
<script src="../assets/js/jquery.shuffle.min.js"></script>
<script src="../assets/js/jquery.stellar.min.js"></script>
<script src="../assets/js/twitterFetcher.min.js"></script>
<script src="../assets/js/imagesloaded.js"></script>
<script src="../assets/js/masonry.pkgd.min.js"></script>
<script src="../assets/flexSlider/jquery.flexslider-min.js"></script>
<script src="../assets/magnific-popup/jquery.magnific-popup.min.js"></script>

<script src="../assets/revolution-version4/js/jquery.themepunch.tools.min.js"></script>
<script src="../assets/revolution-version4/js/jquery.themepunch.revolution.min.js"></script>
<script src="../assets/js/scriptsae52.js?v=5"></script>

<script type="text/javascript">

            jQuery(document).ready(function() {

                jQuery('.tp-banner').show().revolution({

                        dottedOverlay:"none",
                        delay:8000,
                        startwidth:1170,
                        startheight:600,
                        hideThumbs:200,
                        hideTimerBar:"on",
                        
                        thumbWidth:100,
                        thumbHeight:50,
                        thumbAmount:1,
                        
                        navigationType:"bullet",
                        navigationArrows:"solo",
                        navigationStyle:"preview5",
                        
                        touchenabled:"on",
                        onHoverStop:"off",
                        
                        swipe_velocity: 0.7,
                        swipe_min_touches: 1,
                        swipe_max_touches: 1,
                        drag_block_vertical: false,
                                                
                        parallax:"mouse",
                        parallaxBgFreeze:"on",
                        parallaxLevels:[10,7,4,3,2,5,4,3,2,1],
                                                
                        keyboardNavigation:"on",
                        
                                                        
                        shadow:0,
                        fullWidth:"on",
                        fullScreen:"off",

                        spinner:"spinner4"
                });

        }); //ready
        </script>
<script>
            /**
             * ### HOW TO CREATE A VALID ID TO USE: ###
             * Go to www.twitter.com and sign in as normal, go to your settings page.
             * Go to "Widgets" on the left hand side.
             * Create a new widget for what you need eg "user time line" or "search" etc.
             * Feel free to check "exclude replies" if you don't want replies in results.
             * Now go back to settings page, and then go back to widgets page and
             * you should see the widget you just created. Click edit.
             * Look at the URL in your web browser, you will see a long number like this:
             * 613424231099953152
             * Use this as your ID below instead!
             */
             
            if ($('#twitterfeed').length > 0) {
                var twitterWidgetConfig = {
                    id: "613424231099953152", 
                    domId: "twitterfeed",
                    maxTweets: 2,
                    enableLinks: true,
                    showUser: true,
                    showTime: true,
                    showInteraction: false
                };

                twitterFetcher.fetch(twitterWidgetConfig);
            }

        </script>
</body>

<!-- Mirrored from trendytheme.net/demo/matrox/construction-index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 15 Nov 2018 09:27:12 GMT -->
</html>