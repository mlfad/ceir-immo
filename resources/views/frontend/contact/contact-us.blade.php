<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from trendytheme.net/demo/matrox/contact-us.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 15 Nov 2018 09:28:46 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="materialize is a material design based mutipurpose responsive template">
<meta name="keywords" content="material design, card style, material template, portfolio, corporate, business, creative, agency">
<meta name="author" content="trendytheme.net">
<title>Contacts</title>

<link rel="shortcut icon" href="../../assets/img/ico/favicon.png">

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../assets/img/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../assets/img/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="../../assets/img/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="../../assets/img/ico/apple-touch-icon-57-precomposed.png">
<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500,700,900' rel='stylesheet' type='text/css'>

<link href="../../assets/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">

<link href="../../assets/fonts/iconfont/material-icons.css" rel="stylesheet">

<link href="../../assets/owl.carousel/../../assets/owl.carousel.css" rel="stylesheet">
<link href="../../assets/owl.carousel/../../assets/owl.theme.default.min.css" rel="stylesheet">

<link href="../../assets/flexSlider/flexslider.css" rel="stylesheet">

<link href="../../assets/materialize/css/materialize.min.css" rel="stylesheet">

<link href="../../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<link href="../../assets/css/shortcodes/shortcodesae52.css?v=5" rel="stylesheet">

<link href="styleae52.css?v=5" rel="stylesheet">


<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>
<body id="top" class="has-header-search">




<header id="header" class="tt-nav nav-center-align">
    <div class="light-header">
        <div class="container mainmenu">
            <div id="materialize-menu" class="menuzord">
            <!--a href="index.html" class="logo-brand">
            <img src="../assets/img/construction/logo.png" alt="" />
            </a-->
            <ul class="menuzord-menu border-top" id="menu-list">
                        <li style='float:left'><a href="#"><img style="height:100px;" src="assets/img/construction/logo.png" alt="" /></a></li>
                        <li style='padding-top:30px' ><a href="http://ceir-immo.test/">ACCUEIL</a></li>
                        <li style='padding-top:30px'><a href="{{ route('presentation.talk') }}">PRESENTATION</a>
                        </li>
                        <li style='padding-top:30px'><a href="javascript:void(0)">NOS OFFRES</a>
                            <ul class="dropdown">
                                <li><a href="{{ route('offre.terrain') }}">Offres terrains</a></li>
                                <li><a href="{{ route('offre.logement') }}">Offres logements</a></li>
                            </ul>
                        </li>
                        <li style='padding-top:30px'><a href="construction-contact.html">PROJETS</a></li>
                        <li style='padding-top:30px' class="active"><a href="{{ route('contact.contact-us') }}">CONTACTS</a></li>
                        <li style='float:right; padding-top:27px;'><button type="button"  class="btn btn-primary btn-sm">RESERVATION</button></li>
                    </ul>
            </div>
        </div>

        <div class="menu-appear-alt">
            <div class="container">
                <div id="materialize-menu-alt" class="menuzord">
                </div>
            </div>
        </div>

    </div>
</header>



<section class="section-padding">
<div class="container">
<div class="text-center mb-40">
<h2 class="section-title text-uppercase">Contactez-nous</h2>
</div>
<div class="row">
<div class="col-md-8">
<form  action="{{ route('contact.store') }}" method="post">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="row">
<div class="col-md-6">
<div class="input-field">
<input type="text" name="name" class="validate" >
<label for="name">Nom</label>
</div>
</div>
<div class="col-md-6">
<div class="input-field">
<label class="sr-only" for="email">Email</label>
<input  type="email" name="email" class="validate">
<label for="email" data-error="wrong" data-success="right">Email</label>
</div>
</div>
</div>
<div class="row">
<div class="col-md-6">
<div class="input-field">
<input  type="tel" name="phone" class="validate">
<label for="phone">Numero de téléphone</label>
</div>
</div>
<div class="col-md-6">
<div class="input-field">
<input  type="text" name="url" class="validate">
<label for="website">Votre site web</label>
</div>
</div>
</div>
<div class="input-field">
<textarea name="comment" class="materialize-textarea"></textarea>
<label for="message">Message</label>
</div>
<button type="submit" name="submit" class="waves-effect waves-light btn submit-button pink mt-30 pull-right">Envoyer Message</button>
</form>
</div>
<div class="col-md-4 contact-info">
<address>
<i class="material-icons brand-color">&#xE55F;</i>
<div class="address">
Yopougon en face du complexe sportif<br>
Abidjan, Côte d'Ivoire
<br><br><br><br>
<!--p>Hoffman Parkway, P.O Box 154 Mountain View.<br>
United States of America.</p-->
</div>
<i class="material-icons brand-color">&#xE61C;</i>
<div class="phone">
<p>Fax: (00225) 21 79 41 42<br>
Phone: (00225) 41 32 39 07</p>
</div>
<i class="material-icons brand-color">&#xE0E1;</i>
<div class="mail">
<p><a href="mailto:#">info@ceir-immo.com</a><br>
<a href="#">www.ceir-immo.com</a></p>
</div>
</address>
</div>
</div>
</div>
</section>


<!--div id="myMap" class="height-350"></div-->

<footer class="footer footer-one">
<div class="secondary-footer dark-bg darken-1" style="text-align:center;">
<div style:text-color:white; class="container">
<span class="copy-text">Copyright &copy; 2020 <a href="#">CEIR</a> &nbsp; | &nbsp; Tous droits reservés &nbsp; | &nbsp; Developpé par <a href="#">oora.solution</a></span>
</div>
</div>
</footer>

<div id="preloader">
<div class="preloader-position">
<img src="assets/img/logo-colored.jpg" alt="logo">
<div class="progress">
<div class="indeterminate"></div>
</div>
</div>
</div>


<script src="../../assets/js/jquery-2.1.3.min.js"></script>
<script src="../../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../assets/materialize/js/materialize.min.js"></script>
<script src="../../assets/js/menuzord.js"></script>
<script src="../../assets/js/bootstrap-tabcollapse.min.js"></script>
<script src="../../assets/js/jquery.easing.min.js"></script>
<script src="../../assets/js/jquery.sticky.min.js"></script>
<script src="../../assets/js/smoothscroll.min.html"></script>
<script src="../../assets/js/jquery.stellar.min.js"></script>
<script src="../../assets/js/jquery.inview.min.js"></script>
<script src="../../assets/owl.carousel/owl.carousel.min.js"></script>
<script src="../../assets/flexSlider/jquery.flexslider-min.js"></script>
<script src="../../assets/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js"></script>
<script src="../../assets/js/scriptsae52.js?v=5"></script>

<script type="text/javascript">
            jQuery(document).ready(function() {

                //set your google maps parameters
                var $latitude = 40.716304, //Visit http://www.latlong.net/convert-address-to-lat-long.html for generate your Lat. Long.
                    $longitude = -73.995763,
                    $map_zoom = 16 /* ZOOM SETTING */

                //google map custom marker icon 
                var $marker_url = '../../assets/img/pin.png';

                //we define here the style of the map
                var style = [{
                    "stylers": [{
                        "hue": "#03a9f4"
                    }, {
                        "saturation": 10
                    }, {
                        "gamma": 2.15
                    }, {
                        "lightness": 12
                    }]
                }];

                //set google map options
                var map_options = {
                    center: new google.maps.LatLng($latitude, $longitude),
                    zoom: $map_zoom,
                    panControl: true,
                    zoomControl: true,
                    mapTypeControl: true,
                    streetViewControl: true,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    scrollwheel: false,
                    styles: style
                }
                //inizialize the map
                var map = new google.maps.Map(document.getElementById('myMap'), map_options);
                //add a custom marker to the map                
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(5.3295615, -4.0737135),
                    map: map,
                    visible: true,
                    icon: $marker_url
                });

                var contentString = '<div id="mapcontent">' + '<p>materialize, 1355 Market Street, San Francisco.</p></div>';
                var infowindow = new google.maps.InfoWindow({
                    maxWidth: 320,
                    content: contentString
                });

                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open(map, marker);
                });
            });
        </script>
</body>

<!-- Mirrored from trendytheme.net/demo/matrox/contact-us.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 15 Nov 2018 09:28:46 GMT -->
</html>