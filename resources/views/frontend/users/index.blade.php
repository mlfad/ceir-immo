@extends('layouts.app')

@section('content')
  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"> <i class="fas fa-fw fa-users"></i> Utilisateurs</h1>
  </div>

  @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
  @endif


  <!-- DataTales Example style="background-color:white;"-->
  <div class="card shadow mb-4">
    <div class="card-header py-2">
      <h6 class="m-0 font-weight-bold">
        Liste des utilisateurs 
        <span style="float:right">
          <a style="color:gray" href="{{ route('users.create') }}">
            <i class="fas fa-fw fa-user-plus"></i>
          </a>
        </span>
      </h6>
    </div>
    <div class="card-body">
    <div class="table-responsive">
      <table class="table table-hover" id="dataTable" width="100%" >
          <thead>
            <tr>
              <th>No</th>
              <th>ID</th>
              <th>Nom et Prenom</th>
              <th>Email</th>
              <th>Contact</th>
              <th>Type</th>
              <th>Etat</th>
              <th width="10%">Action</th>
            </tr>
          </thead>
          <tbody>
          @foreach ($data as $key => $user)
            <tr>
              <td>{{ ++$i }}</td>
              <td>{{ 3000 + $user->id }}</td>
              <td>{{ $user->last_name }} {{ $user->first_name }}</td>
              <td>{{ $user->email }}</td>
              <td>{{ $user->contact_1 }}</td>
              <td>{{ $user->usertype }}</td>
              <td>{{ $user->status }}</td>
              <td>
                <form action="{{ route('users.destroy',$user->id) }}" method="POST">
                  @csrf
                    @method('DELETE')
                  <button type="submit" class="btn btn-danger"><i  class="fas fa-trash"></i></button>
                </form>
              </td>
            </tr>
          @endforeach
          </tbody>     
      </table>
    </div>
    </div>
  </div>
@endsection