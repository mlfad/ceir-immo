@extends('layouts.app')
@section('content')
<div class="col-lg-12">
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"> <i class="fas fa-fw fa-user"></i>
         Membres</h1>
    </div>
	@if ($errors->any())
		<div class="alert alert-danger">
			<strong>Ooops!</strong> <br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
		</div>
	@endif
	<div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">Modifier un utilisateur</h6>
        </div>
        <div class="card-body">
			<form action="{{ route('users.update',$user->id) }}" method="POST">
				@csrf
				<div class="row">
				    <div class="col-xs-6 col-sm-6 col-md-6">
						<div class="form-group">
							<strong>Type :</strong>
							<select id="inputState" class="form-control" name="type_user_id">
								<option selected>Selectionner...</option>
								<option  value="1">Activer</option>
								<option  value="2">Desactiver</option>
							</select>
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6">
						<div class="form-group">
							<strong>Nom :</strong>
							<input type="text" name="first_name" value="{{ $user->first_name }}" class="form-control" placeholder="">
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6">
						<div class="form-group">
							<strong>Prenom :</strong>
							<input type="text" name="last_name" value="{{ $user->last_name }}" class="form-control" placeholder="">
						</div>
					</div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
						<div class="form-group">
							<strong>Pseudo :</strong>
							<input type="text" name="last_name" value="{{ $user->username }}" class="form-control" placeholder="">
						</div>
					</div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
						<div class="form-group">
							<strong>Email :</strong>
							<input type="text" name="email"  value="{{ $user->email }}"  class="form-control" placeholder="">
						</div>
					</div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
						<div class="form-group">
							<strong>Contact 1 :</strong>
							<input type="text" name="contact_1"  value="{{ $user->contact_1 }}" class="form-control" placeholder="">
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6">
						<div class="form-group">
							<strong>Contact 2 :</strong>
							<input type="text" name="contact_2"  value="{{ $user->contact_2 }}" class="form-control" placeholder="">
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6">
						<div class="form-group">
							<strong>Status :</strong>
							<select id="inputState" class="form-control" name="status_id">
								<option selected>Selectionner...</option>
								<option  value="1">Activer</option>
								<option  value="2">Desactiver</option>
							</select>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12">
					       <a class="btn btn-primary" href="{{ route('users.index') }}">Retour</a>
							<button type="submit"  style='float:right' class="btn btn-success">Valider</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection