@extends('layouts.app')


@section('content')
<div class="col-lg-12">
	<div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"> <i class="fas fa-fw fa-user-plus"></i>
         Utilisateur</h1>
    </div>
	@if ($errors->any())
		<div class="alert alert-danger">
			<strong>Ooops!</strong> <br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
		</div>
	@endif
	<div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold">Ajouter un utilisateur</h6>
        </div>
        <div class="card-body">
			<form action="{{ route('users.store') }}" method="POST">
				@csrf
				<div class="row">
				    <div class="col-xs-6 col-sm-6 col-md-6">
						<div class="form-group">
							<strong>Type d'utilisateur :</strong>
							<select id="inputState" class="form-control" name="type_user_id">
								<option  value="RMR">Responsable de mise en oeuvre</option>
								<option  value="Auditeur">Auditeur interne</option>
							</select>
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6">
						<div class="form-group">
							<strong>Status :</strong>
							<select id="inputState" class="form-control" name="status_id">
								<option  value="Actif">Actif</option>
								<option  value="Inactif">Inactif</option>
							</select>
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6">
						<div class="form-group">
							<strong>Nom :</strong>
							<input type="text" name="last_name" class="form-control" placeholder="">
						</div>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6">
						<div class="form-group">
							<strong>Prenom :</strong>
							<input type="text" name="first_name" class="form-control" placeholder="">
						</div>
					</div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
						<div class="form-group">
							<strong>Email :</strong>
							<input type="text" name="email" class="form-control" placeholder="">
						</div>
					</div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
						<div class="form-group">
							<strong>Contact :</strong>
							<input type="text" name="contact_1" class="form-control" placeholder="">
						</div>
					</div>
					
					

					<div class="col-xs-12 col-sm-12 col-md-12 ">
					        <a href="{{ route('users.index') }}" class="btn btn-primary">Retour</a>
							<button style="float:right" type="submit" class="btn btn-success">Valider</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection