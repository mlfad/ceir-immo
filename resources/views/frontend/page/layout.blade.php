<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from trendytheme.net/demo/matrox/construction-index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 15 Nov 2018 09:26:48 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="materialize is a material design based mutipurpose responsive template">
<meta name="keywords" content="material design, card style, material template, portfolio, corporate, business, creative, agency">
<meta name="author" content="trendytheme.net">
<title>CEIR - Immobilier - Transport</title>

<link rel="shortcut icon" href="assets/img/ico/favicon.jpg">

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/img/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/img/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/img/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="assets/img/ico/apple-touch-icon-57-precomposed.png">
<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500,700,900' rel='stylesheet' type='text/css'>

<link href="assets/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">

<link href="assets/fonts/iconfont/material-icons.css" rel="stylesheet">

<link href="assets/magnific-popup/magnific-popup.css" rel="stylesheet">

<link href="assets/flexSlider/flexslider.css" rel="stylesheet">

<link href="assets/materialize/css/materialize.min.css" rel="stylesheet">

<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<link href="assets/css/shortcodes/shortcodes.css" rel="stylesheet">

<link href="style.css" rel="stylesheet">

<link href="assets/css/skins/construction.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="assets/revolution-version4/css/extralayers.css" media="screen">
<link rel="stylesheet" type="text/css" href="assets/revolution-version4/css/settings.css" media="screen">


<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body id="top">
<!-- Début menu-->
<nav class="navbar navbar-expand-lg navbar-light bg-dark">
    <a class="navbar-brand" href="./index.html">Gestion des articles</a>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item active">
              <a class="nav-link" href="./index.html">Accueil</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="pages/articles.html">Artciles</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="pages/commandes.html">Commandes</a>
            </li>
        </ul>
    </div>
</nav>


@yield('content')

<footer class="footer footer-one">
    <div class="secondary-footer dark-bg darken-1" style="text-align:center;">
        <div class="container">
             <span class="copy-text">Copyright &copy; 2020 <a href="#">CEIR</a> &nbsp; | &nbsp; Tous droits reservés &nbsp; | &nbsp; Developpé par <a href="#">oora.solution</a></span>
        </div>
    </div>
</footer>

<script src="assets/js/jquery-2.1.3.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/materialize/js/materialize.min.js"></script>
<script src="assets/js/jquery.easing.min.js"></script>
<script src="assets/js/smoothscroll.min.html"></script>
<script src="assets/js/menuzord.js"></script>
<script src="assets/js/equalheight.js"></script>
<script src="assets/js/bootstrap-tabcollapse.min.js"></script>
<script src="assets/js/jquery.inview.min.js"></script>
<script src="assets/js/jquery.countTo.min.js"></script>
<script src="assets/js/jquery.shuffle.min.js"></script>
<script src="assets/js/jquery.stellar.min.js"></script>
<script src="assets/js/twitterFetcher.min.js"></script>
<script src="assets/js/imagesloaded.js"></script>
<script src="assets/js/masonry.pkgd.min.js"></script>
<script src="assets/flexSlider/jquery.flexslider-min.js"></script>
<script src="assets/magnific-popup/jquery.magnific-popup.min.js"></script>

<script src="assets/revolution-version4/js/jquery.themepunch.tools.min.js"></script>
<script src="assets/revolution-version4/js/jquery.themepunch.revolution.min.js"></script>
<script src="assets/js/scriptsae52.js?v=5"></script>

</body>

</html>