<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from trendytheme.net/demo/matrox/construction-about-us.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 15 Nov 2018 09:30:55 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="materialize is a material design based mutipurpose responsive template">
<meta name="keywords" content="material design, card style, material template, portfolio, corporate, business, creative, agency">
<meta name="author" content="trendytheme.net">
<title>Materialize Material Design HTML Construction Template</title>

<link rel="shortcut icon" href="assets/img/ico/favicon.png">

<link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/img/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/img/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/img/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="assets/img/ico/apple-touch-icon-57-precomposed.png">
<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,500,700,900' rel='stylesheet' type='text/css'>

<link href="assets/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">

<link href="assets/fonts/iconfont/material-icons.css" rel="stylesheet">

<link href="assets/magnific-popup/magnific-popup.css" rel="stylesheet">

<link href="assets/flexSlider/flexslider.css" rel="stylesheet">

<link href="assets/materialize/css/materialize.min.css" rel="stylesheet">

<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<link href="assets/css/shortcodes/shortcodes.css" rel="stylesheet">

<link href="style.css" rel="stylesheet">

<link href="assets/css/skins/construction.css" rel="stylesheet">


<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>
<body id="top">
<header id="header" class="tt-nav nav-center-align">
    <div class="light-header">
        <div class="container mainmenu">
            <div id="materialize-menu" class="menuzord">
            <!--a href="index.html" class="logo-brand">
            <img src="assets/img/construction/logo.png" alt="" />
            </a-->
                    <ul class="menuzord-menu border-top" id="menu-list">
                        <li style='float:left'><a href="construction-index.html"><img style="height:100px;" src="assets/img/construction/logo.png" alt="" /></a></li>
                        <li style='padding-top:30px' class="active"><a href="construction-index.html">ACCUEIL</a></li>
                        <li style='padding-top:30px'><a href="javascript:void(0)">PRESENTATION</a>
                            <ul class="dropdown">
                                <li><a href="construction-portfolio-4.html">Mot du DG</a></li>
                                <li><a href="construction-portfolio-3.html">Presentation CEIR</a></li>
                            </ul>
                        </li>
                        <li style='padding-top:30px'><a href="javascript:void(0)">NOS OFFRES</a>
                            <ul class="dropdown">
                                <li><a href="construction-portfolio-4.html">Offres terrains</a></li>
                                <li><a href="construction-portfolio-3.html">Offres logements</a></li>
                            </ul>
                        </li>
                        <li style='padding-top:30px'><a href="construction-contact.html">PROJETS</a></li>
                        <li style='padding-top:30px'><a href="construction-contact.html">CONTACTS</a></li>
                        <li style='padding-top:30px'><a href="construction-service.html">FAQS</a></li>
                        <li style='float:right; padding-top:27px;'><button type="button"  class="btn btn-primary btn-sm">RESERVATION</button></li>
                    </ul>
            </div>
        </div>

        <div class="menu-appear-alt">
            <div class="container">
                <div id="materialize-menu-alt" class="menuzord">
                </div>
            </div>
        </div>

    </div>
</header>

<section class="page-title page-title-bg ptb-70">
<div class="container">
<div class="row">
<div class="col-md-12">
<h2 class="text-uppercase">About Us</h2>
<span>A little bit about us</span>
<ol class="breadcrumb">
<li><a href="#">Home</a></li>
<li class="active">About Us</li>
</ol>
</div>
</div>
</div>
</section>

<section class="page-title page-title-bg ptb-70">
<div class="container">
<div class="row">
<div class="col-md-12">
<h2 class="text-uppercase">About Us</h2>
<span>A little bit about us</span>
<ol class="breadcrumb">
<li><a href="#">Home</a></li>
<li class="active">About Us</li>
</ol>
</div>
</div>
</div>
</section>

<section class="padding-top-110 padding-bottom-90">
<div class="container">
<div class="row">
<div class="col-md-6">
<h2 class="text-bold mb-30">About Us</h2>
<p>Quickly procrastinate functionalized human capital with equity invested experiences. Rapidiously provide access to extensible solutions after pandemic supply chains. Credibly supply resource sucking channels before areas Dynamically harness cooperative partnerships rather than <a href="#">just in time total linkage</a>. Dramatically syndicate plug and play professional with focused. Credibly supply resource sucking channels before areas.</p>
<p>Lorem ipsum dolor sit amet cr adipiscing elit. <u>In maximus</u> ligula semper metus pellentesque mattis.</p>
</div>
<div class="col-md-6">
<h2 class="text-bold mb-30">Our Skills</h2>
<div class="progress-section">
<span class="progress-title">Web Design</span>
<div class="progress">
<div class="progress-bar brand-bg six-sec-ease-in-out" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
<span>90%</span>
</div>
</div>
</div> 
<div class="progress-section">
<span class="progress-title">Mobile Interface</span>
<div class="progress">
<div class="progress-bar brand-bg six-sec-ease-in-out" role="progressbar" aria-valuenow="86" aria-valuemin="0" aria-valuemax="100">
<span>86%</span>
</div>
</div>
</div> 
<div class="progress-section">
<span class="progress-title">UX Design</span>
<div class="progress">
<div class="progress-bar brand-bg six-sec-ease-in-out" role="progressbar" aria-valuenow="86" aria-valuemin="0" aria-valuemax="100">
<span>86%</span>
</div>
</div>
</div> 
<div class="progress-section">
<span class="progress-title">Print Design</span>
<div class="progress">
<div class="progress-bar brand-bg six-sec-ease-in-out" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
<span>90%</span>
</div>
</div>
</div> 
</div>
</div>
</div>
</section>
<hr>
<section class="section-padding">
<div class="container">
<div class="text-center mb-80">
<h2 class="section-title text-bold">Intarective Team</h2>
<p class="section-sub">Quisque non erat mi. Etiam congue et augue sed tempus. Aenean sed ipsum luctus, scelerisque ipsum nec, iaculis justo. Sed at vestibulum purus, sit amet viverra diam nulla ac nisi rhoncus.</p>
</div>
<div class="row">
<div id="teamGrid">
<div class="col-md-3 col-sm-6 col-xs-12 single-team">
<div class="team-wrapper text-center">
<div class="team-img">
<a href="#"><img src="assets/img/construction/team/team-1.jpg" class="img-responsive" alt="Image"></a>
</div>
<div class="team-title">
<h3><a href="#">John Smith</a></h3>
<span>Structural Engineer</span>
</div>
<ul class="team-social-links list-inline">
<li><a href="#"><i class="fa fa-facebook"></i></a></li>
<li><a href="#"><i class="fa fa-twitter"></i></a></li>
<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
<li><a href="#"><i class="fa fa-envelope-o"></i></a></li>
</ul>
</div>
</div>
<div class="col-md-3 col-sm-6 col-xs-12 single-team">
<div class="team-wrapper text-center">
<div class="team-img">
<a href="#"><img src="assets/img/construction/team/team-2.jpg" class="img-responsive" alt="Image"></a>
</div>
<div class="team-title">
<h3><a href="#">Martina Doe</a></h3>
<span>Structural Planner</span>
 </div>
<ul class="team-social-links list-inline">
<li><a href="#"><i class="fa fa-facebook"></i></a></li>
<li><a href="#"><i class="fa fa-twitter"></i></a></li>
<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
<li><a href="#"><i class="fa fa-envelope-o"></i></a></li>
</ul>
</div>
</div>
<div class="col-md-3 col-sm-6 col-xs-12 single-team">
<div class="team-wrapper text-center">
<div class="team-img">
<a href="#"><img src="assets/img/construction/team/team-3.jpg" class="img-responsive" alt="Image"></a>
</div>
<div class="team-title">
<h3><a href="#">robert jakis</a></h3>
<span>Concrete Technician</span>
</div>
<ul class="team-social-links list-inline">
<li><a href="#"><i class="fa fa-facebook"></i></a></li>
<li><a href="#"><i class="fa fa-twitter"></i></a></li>
<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
<li><a href="#"><i class="fa fa-envelope-o"></i></a></li>
</ul>
</div>
</div>
<div class="col-md-3 col-sm-6 col-xs-12 single-team">
<div class="team-wrapper text-center">
<div class="team-img">
<a href="#"><img src="assets/img/construction/team/team-4.jpg" class="img-responsive" alt="Image"></a>
</div>
<div class="team-title">
<h3><a href="#">ketty pary</a></h3>
<span>Civil Engineer</span>
</div>
<ul class="team-social-links list-inline">
<li><a href="#"><i class="fa fa-facebook"></i></a></li>
<li><a href="#"><i class="fa fa-twitter"></i></a></li>
<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
<li><a href="#"><i class="fa fa-envelope-o"></i></a></li>
</ul>
</div>
</div>
<div class="col-md-3 col-sm-6 col-xs-12 single-team">
<div class="team-wrapper text-center">
<div class="team-img">
<a href="#"><img src="assets/img/construction/team/team-5.jpg" class="img-responsive" alt="Image"></a>
</div>
<div class="team-title">
<h3><a href="#">Albama Smith</a></h3>
<span>Civil Engineer</span>
</div>
<ul class="team-social-links list-inline">
<li><a href="#"><i class="fa fa-facebook"></i></a></li>
<li><a href="#"><i class="fa fa-twitter"></i></a></li>
<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
<li><a href="#"><i class="fa fa-envelope-o"></i></a></li>
</ul>
</div>
</div>
<div class="col-md-3 col-sm-6 col-xs-12 single-team">
<div class="team-wrapper text-center">
<div class="team-img">
 <a href="#"><img src="assets/img/construction/team/team-6.jpg" class="img-responsive" alt="Image"></a>
</div>
<div class="team-title">
<h3><a href="#">Elita Chow</a></h3>
<span>Civil Engineer</span>
</div>
<ul class="team-social-links list-inline">
<li><a href="#"><i class="fa fa-facebook"></i></a></li>
<li><a href="#"><i class="fa fa-twitter"></i></a></li>
<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
<li><a href="#"><i class="fa fa-envelope-o"></i></a></li>
</ul>
</div>
</div>
<div class="col-md-3 col-sm-6 col-xs-12 single-team">
<div class="team-wrapper text-center">
<div class="team-img">
<a href="#"><img src="assets/img/construction/team/team-7.jpg" class="img-responsive" alt="Image"></a>
</div>
<div class="team-title">
<h3><a href="#">Eftekhar jakis</a></h3>
<span>Civil Engineer</span>
</div>
<ul class="team-social-links list-inline">
<li><a href="#"><i class="fa fa-facebook"></i></a></li>
<li><a href="#"><i class="fa fa-twitter"></i></a></li>
<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
<li><a href="#"><i class="fa fa-envelope-o"></i></a></li>
</ul>
</div>
</div>
<div class="col-md-3 col-sm-6 col-xs-12 single-team">
<div class="team-wrapper text-center">
<div class="team-img">
<a href="#"><img src="assets/img/construction/team/team-8.jpg" class="img-responsive" alt="Image"></a>
</div>
<div class="team-title">
<h3><a href="#">Kupa Samsu</a></h3>
<span>Civil Engineer</span>
</div>
<ul class="team-social-links list-inline">
<li><a href="#"><i class="fa fa-facebook"></i></a></li>
<li><a href="#"><i class="fa fa-twitter"></i></a></li>
<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
<li><a href="#"><i class="fa fa-envelope-o"></i></a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</section>
<hr>
<section class="section-padding">
<div class="container">
<div class="text-center mb-80">
<h2 class="section-title text-bold">Mission & Vision</h2>
<p class="section-sub">Quisque non erat mi. Etiam congue et augue sed tempus. Aenean sed ipsum luctus, scelerisque ipsum nec, iaculis justo. Sed at vestibulum purus, sit amet viverra diam nulla ac nisi rhoncus.</p>
</div>
<div class="video-intro mt-50">
<img src="assets/img/construction/video-bg.jpg" class="img-responsive " alt="Image">
<a class="external-link popup-video" href="https://www.youtube.com/watch?v=A1CoJKju-YI" title=""><i class="material-icons">&#xE038;</i></a>
</div>
</div>
</section>

<footer class="footer footer-one">
<div class="primary-footer dark-bg lighten-3">
<div class="container">
<a href="#top" class="page-scroll btn-floating btn-large brand-bg back-top waves-effect waves-light tt-animate btt" data-section="#top">
<i class="material-icons">&#xE316;</i>
</a>
<div class="row">
<div class="col-md-4 widget clearfix">
<h2 class="white-text">Qui sommes-nous?</h2>
<p style="text-align:justify;">Eleifend auctor condimentum luctus ac aenean bibendum erat facilisi hac dignissim himenaeos per hendrerit adipiscing varius lectus imperdiet cubilia dignissim dictumst
Eleifend auctor condimentum luctus ac aenean bibendum erat facilisi hac dignissim himenaeos per hendrerit adipiscing varius lectus imperdiet cubilia dignissim dictumst
Eleifend auctor condimentum luctus ac aenean bibendum erat facilisi hac dignissim himenaeos per hendrerit adipiscing varius lectus imperdiet cubilia dignissim dictumst.</p>
<ul class="social-link tt-animate ltr">
<li><a href="#"><i class="fa fa-facebook"></i></a></li>
<li><a href="#"><i class="fa fa-twitter"></i></a></li>
<li><a href="#"><i class="fa fa-tumblr"></i></a></li>
<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
<li><a href="#"><i class="fa fa-instagram"></i></a></li>
<li><a href="#"><i class="fa fa-rss"></i></a></li>
</ul>
</div>
<div class="col-md-3 widget">
<h2 class="white-text">Liens important</h2>
<ul class="footer-list">
<li><a href="#">A propos de nous</a></li>
<li><a href="#">Services</a></li>
<li><a href="#">Terms &amp; Condition</a></li>
<li><a href="#">Privacy Policy</a></li>
<li><a href="#">Nous contacter</a></li>
</ul>
</div>
<div class="col-md-5">
<h2 class="white-text">Suivez nous</h2>
<form name="contact-form" id="contactForm" action="https://trendytheme.net/demo/matrox/sendemail.php" method="POST">
<div class="row">
<div class="col-md-6">
<div class="input-field">
<input type="text" name="name" class="validate" id="name">
<label for="name">Nom et Prenom*</label>
</div>
</div>
<div class="col-md-6">
<div class="input-field">
<input id="email" type="email" name="email" class="validate">
<label for="email" data-error="wrong" data-success="right">Adresse Email*</label>
</div>
</div>
</div>
<div class="input-field">
<textarea name="message" id="message" class="materialize-textarea"></textarea>
<label for="message">Votre Message</label>
</div>
<div class="text-center">
<button type="submit" name="submit" class="waves-effect waves-light btn brand-bg text-medium mt-30">Envoyer Message</button>
</div>
</form>
</div>
</div>
</div>
</div>
<div class="secondary-footer dark-bg darken-1" style="text-align:center;">
<div class="container">
<span class="copy-text">Copyright &copy; 2020 <a href="#">CEIR</a> &nbsp; | &nbsp; Tous droits reservés &nbsp; | &nbsp; Developpé par <a href="#">oora.solution</a></span>
</div>
</div>
</footer>

<div id="preloader">
<div class="preloader-position">
<img src="assets/img/logo-colored.jpg" alt="logo">
<div class="progress">
<div class="indeterminate"></div>
</div>
</div>
</div>


<script src="assets/js/jquery-2.1.3.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/materialize/js/materialize.min.js"></script>
<script src="assets/js/jquery.easing.min.js"></script>
<script src="assets/js/smoothscroll.min.html"></script>
<script src="assets/js/menuzord.js"></script>
<script src="assets/js/imagesloaded.js"></script>
<script src="assets/js/equalheight.js"></script>
<script src="assets/js/bootstrap-tabcollapse.min.js"></script>
<script src="assets/js/jquery.inview.min.js"></script>
<script src="assets/js/jquery.countTo.min.js"></script>
<script src="assets/js/jquery.shuffle.min.js"></script>
<script src="assets/js/jquery.stellar.min.js"></script>
<script src="assets/js/twitterFetcher.min.js"></script>
<script src="assets/js/masonry.pkgd.min.js"></script>
<script src="assets/flexSlider/jquery.flexslider-min.js"></script>
<script src="assets/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="assets/js/scriptsae52.js?v=5"></script>
<script>
            /**
             * ### HOW TO CREATE A VALID ID TO USE: ###
             * Go to www.twitter.com and sign in as normal, go to your settings page.
             * Go to "Widgets" on the left hand side.
             * Create a new widget for what you need eg "user time line" or "search" etc.
             * Feel free to check "exclude replies" if you don't want replies in results.
             * Now go back to settings page, and then go back to widgets page and
             * you should see the widget you just created. Click edit.
             * Look at the URL in your web browser, you will see a long number like this:
             * 613424231099953152
             * Use this as your ID below instead!
             */
             
            if ($('#twitterfeed').length > 0) {
                var twitterWidgetConfig = {
                    id: "613424231099953152", 
                    domId: "twitterfeed",
                    maxTweets: 2,
                    enableLinks: true,
                    showUser: true,
                    showTime: true,
                    showInteraction: false
                };

                twitterFetcher.fetch(twitterWidgetConfig);
            }

        </script>
</body>

<!-- Mirrored from trendytheme.net/demo/matrox/construction-about-us.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 15 Nov 2018 09:31:00 GMT -->
</html>