<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Offre;
use DB;

class OffreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $data = Offre::all();
        return view('admin.offre.index',compact('data'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function terrain()
    {
        $data = Offre::where('type', 'terrain')->get();
        return view('frontend.offre.terrain',compact('data'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function logement()
    {
        $data = Offre::where('type', 'logement')->get();
        return view('frontend.offre.logement',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function immo()
    {
        return view('admin.offre.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'designation'      => 'required',
            'montant'          => 'required',
            'situation_geo'    => 'required',
            'superficie'       => 'required',
        ]);

        $image_one = $request->file('image_un');
        //dd($request->file('image_un'));
        $new_name_one = rand().'.'. $image_one->getClientOriginalExtension();
        //dd($new_name_one);
        $image_one->move(public_path('images'), $new_name_one);

        $image_two = $request->file('image_deux');
        $new_name_two = rand().'.'. $image_two->getClientOriginalExtension();
        $image_two->move(public_path('images'), $new_name_two);

        $image_three = $request->file('image_trois');
        $new_name_three = rand().'.'. $image_three->getClientOriginalExtension();
        $image_three->move(public_path('images'), $new_name_three);

        $image_for = $request->file('image_q');
        $new_name_for = rand().'.'. $image_for->getClientOriginalExtension();
        $image_for->move(public_path('images'), $new_name_for);

        $data = array(
            'type'             =>   $request->type,
            'designation'         =>   $request->designation,
            'superficie'          =>   $request->superficie,
            'montant'             =>   $request->montant,
            'situation_geo'       =>   $request->situation_geo,
            'image_un'            =>   $new_name_one,
            'image_deux'          =>   $new_name_two,
            'image_trois'         =>   $new_name_three,
            'image_q'             =>   $new_name_for,
            'detail'              =>   $request->detail
        );
  
        Offre::create($data);
   
        return redirect()->route('offre.all')
                        ->with('success','Offre created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('offre.show',compact('offre'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('offre.edit',compact('offre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'type_id'          => 'required',
            'designation'      => 'required',
            'montant'          => 'required',
            'situation_geo'    => 'required',
            'superficie'       => 'required',
            'images'           => 'required',
            'detail_id'        => 'required'
        ]);
  
        $offre->update($request->all());
  
        return redirect()->route('offre.index')
                        ->with('success','Offre updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Offre::findOrFail($id)->delete();
  
        return redirect()->route('offre.all')
                        ->with('success','Offre  supprimé avec succès!');
    }
}
