<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offre extends Model
{
    protected $fillable = [
        'type',
        'designation',
        'superficie',
        'montant',
        'situation_geo',
        'image_un',
        'image_deux',
        'image_trois',
        'image_q',
        'detail'
    ];

    

}
