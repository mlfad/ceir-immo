<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
Route::get('/', function(){
    return view('construction-index')->name('accueil');
});
*/

Route::get('/','AccueilController@home')->name('home');
Route::get('/presentation/talk','PresentationController@talk')->name('presentation.talk');
Route::get('/contact','ContactController@create')->name('contact.contact-us');
Route::get('/projet','ProjetController@index')->name('projet');
Route::post('/contact/store','ContactController@store')->name('contact.store');
Route::get('/offres/logement','OffreController@logement')->name('offre.logement');
Route::get('/offres/terrain','OffreController@terrain')->name('offre.terrain');
Route::get('/booking/all','ReservationController@index')->name('booking');


Route::get('/admin/login', function () {return view('login');});
Route::get('/admin/add/immo','OffreController@immo')->name('offre.create');
Route::get('/admin/contact/all','ContactController@index')->name('contact.all');
Route::DELETE('admin/contact/delete/{id}', 'ContactController@destroy')->name('contact.destroy');
Route::get('admin/offre/all','OffreController@all')->name('offre.all');
Route::post('admin/offre/store','OffreController@store')->name('offre.store');
Route::DELETE('admin/offre/delete/{id}', 'OffreController@destroy')->name('offre.destroy');

Route::get('admin/roles-all', 'RoleController@index')->name('roles.index');
Route::GET('/admin/roles-new', 'RoleController@create')->name('roles.create');
Route::GET('/admin/roles-{id}-detail', 'RoleController@show')->name('roles.show');
Route::GET('/admin/roles-{id}-edit', 'RoleController@edit')->name('roles.edit');
Route::PATCH('/admin/roles-{id}-update', 'RoleController@update')->name('roles.update');
Route::DELETE('/admin/roles-delete-{id}', 'RoleController@destroy')->name('roles.destroy');
Route::POST('/admin/roles-ajout', 'RoleController@store')->name('roles.store');

Route::get('admin/users/all', 'UserController@index')->name('users.index');
Route::GET('admin/users/new', 'UserController@create')->name('users.create');
Route::GET('admin/users/{id}/detail', 'UserController@show')->name('users.show');
Route::GET('admin/users/{id}/edit', 'UserController@edit')->name('users.edit');
Route::PATCH('admin/users/{id}/update', 'UserController@update')->name('users.update');
Route::DELETE('admin/users/delete/{id}', 'UserController@destroy')->name('users.destroy');
Route::POST('admin/users/create', 'UserController@store')->name('users.store');
