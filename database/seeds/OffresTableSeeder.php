<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

use App\Models\Offre;

class OffresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
    	foreach (range(1,3) as $index) {
        $offre = Offre::create([
            'type_id'         => $faker->numberBetween($min = 1, $max = 4),
            'designation'     => $faker->text(15),
            'montant'         => $faker->numberBetween($min = 15000000, $max = 40000000),
            'situation_geo'   => $faker->randomElement(array('Riviera','Yopougon','Treichville')),
            'superficie'      => $faker->numberBetween($min = 500, $max = 2000),
            'images'          => $faker->randomElement(array('2.png','3.png')),
            'detail_id'       => $faker->numberBetween($min = 1, $max = 30)
        ]);
    
        }  
    }
}
