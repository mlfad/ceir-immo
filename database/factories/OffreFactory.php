<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Offre;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Offre::class, function (Faker $faker) {
    return [
            'type_id'         => $faker->numberBetween($min = 1, $max = 3),
            'designation'     => $faker->text(15),
            'montant'         => $faker->numberBetween($min = 15000000, $max = 40000000),
            'situation_geo'   => $faker->randomElement(array('Riviera','Yopougon','Treichville')),
            'superficie'      => $faker->numberBetween($min = 500, $max = 2000),
            'images'          => $faker->randomElement(array('2.png','3.png')),
            'detail_id'       => $faker->numberBetween($min = 1, $max = 30)
    ];
});
